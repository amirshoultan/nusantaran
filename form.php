<?php

include_once 'layouts/admin/header.php';
include_once 'layouts/admin/navbar.php';
include_once 'layouts/admin/sidebar.php';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Form Data</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item">Home</li>
          <li class="breadcrumb-item active"><a href="#">Form Data</a></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Destinasi Wisata</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form action="./functions/add.php" method="post" enctype="multipart/form-data">
            <div class="card-body">
              <div class="form-group">
                <label for="nama">Nama Destinasi Wisata</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Destinasi Wisata">
              </div>

              <div class="form-group">
                <label for="pulau">Pulau </label>
                <select class="form-control " id="pulau" name="pulau">
                  <option value="Pulau Sumatera">Pulau Sumatera</option>
                  <option value="Pulau Jawa">Pulau Jawa</option>
                  <option value="Pulau Bali">Pulau Bali</option>
                  <option value="Pulau Papua">Pulau Papua</option>
                </select>
              </div>

              <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <textarea class="form-control" rows="3" placeholder="Masukkan deskripsi..." name="deskripsi"></textarea>
              </div>

              <div class="form-group">
                <label for="link">Link Destinasi Wisata</label>
                <input type="text" class="form-control" id="link" name="link" placeholder="Masukkan Nama Destinasi Wisata">
              </div>

              <div class="form-group">
                <label for="gambar">File input</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="gambar" name="gambar">
                    <label class="custom-file-label" for="gambar">Choose file</label>
                  </div>
                  <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                  </div>
                </div>
              </div>

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.card -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once 'layouts/admin/footer.php' ?>
</div>
<!-- ./wrapper -->
</div>
<!-- ./wrapper -->
<?php include_once 'layouts/admin/script.php' ?>

<!-- Page specific script -->
<script>
  $(function() {
    bsCustomFileInput.init();
  });
</script>
</body>

</html>