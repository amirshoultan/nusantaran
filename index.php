<?php
include_once 'functions/config.php';
include_once 'layouts/header.php';

?>


<body>

  <div class="hero_area">
    <div class="bg-box">
      <img src="images/pantai.jpg" alt="">
    </div>
    <!-- header section strats -->
    <header class="header_section">
      <div class="container">
        <?php
        include_once 'layouts/navbar.php';
        ?>
      </div>
    </header>
    <!-- end header section -->
    <!-- slider section -->
    <section class="slider_section ">
      <div id="customCarousel1" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="container ">
              <div class="row">
                <div class="col-md-7 col-lg-6 ">
                  <div class="detail-box">
                    <h1>
                      5 Destinasi wisata alam tersembunyi di Bali
                    </h1>
                    <p>
                      Saat merencanakan liburan ke pulau Bali, hal pertama yang akan anda cari adalah informasi mengenai objek wisata yang ada di pulau Bali. Setelah anda mendapatkan informasi mengenai tempat wisata menarik di pulau Bali, maka anda akan diskusikan kepada keluarga, teman, pasangan yang akan anda ajak berlibur ke Bali.
                    </p>
                    <div class="btn-box">
                      <a href="" class="btn1">
                        Lihat selengkapnya..
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item ">
            <div class="container ">
              <div class="row">
                <div class="col-md-7 col-lg-6 ">
                  <div class="detail-box">
                    <h1>
                      Kuliner enak di Padang
                    </h1>
                    <p>
                      Makanan khas Padang memang dikenal memiliki cita rasa yang lezat dan pastinya akan menggugah selera makan Anda. Bukan hanya nasi padang yang sudah populer di berbagai kalangan, sebenarnya ada banyak kuliner lain yang tak kalah lezat untuk Anda coba.
                      Aneka masakan khas Padang memang terkenal memiliki rasa yang khas dan kuat. Oleh sebab itu, kebanyakan orang gemar untuk menyantap masakan khas Padang.
                    </p>
                    <div class="btn-box">
                      <a href="" class="btn1">
                        Lihat selengkapnya..
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container ">
              <div class="row">
                <div class="col-md-7 col-lg-6 ">
                  <div class="detail-box">
                    <h1>
                      Oleh-oleh khas Kalimantan terkenal wisatawan mancanegara?
                    </h1>
                    <p>
                      Pulau yang mendapat julukan “Borneo”di kancah internasional ini terbagi dalam beberapa provinsi yakni Kalimantan selatan, Kalimantan Timur dan Kalimantan Tengah. Kesemuanya kaya akan wisata alam dan kebudayaannya yang beragam. Banyak kuliner khas di daerah tersebut yang tidak boleh kamu lewatkan. Jika berkunjung ke sini, pastikan juga kamu tidak lupa untuk membeli sesuatu yang khas sebagai buah tangan atau oleh-oleh dari Kalimantan untuk keluarga tercinta. Berikut ini oleh-oleh khas Kalimantan terkenal menjadi buruan para wisatawan.
                    </p>
                    <div class="btn-box">
                      <a href="" class="btn1">
                        Lihat selengkapnya..
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <ol class="carousel-indicators">
            <li data-target="#customCarousel1" data-slide-to="0" class="active"></li>
            <li data-target="#customCarousel1" data-slide-to="1"></li>
            <li data-target="#customCarousel1" data-slide-to="2"></li>
          </ol>
        </div>
      </div>

    </section>
    <!-- end slider section -->
  </div>

  <!-- offer section -->

  <section class="offer_section layout_padding-bottom">
    <div class="offer_container">
      <div class="container ">
        <div class="row">
          <div class="col-md-6  ">
            <div class="box ">
              <div class="img-box">
                <img src="images/ubud2.jpg" alt="">
              </div>
              <div class="detail-box">
                <h5>
                  Ubud Bali
                </h5>
                <h6>
                  <span>Best Review</span>
                </h6>
                <a href="https://goo.gl/maps/TyRAoUMW3M23WHrP8">
                  Lihat Selengkapnya... <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                    <g>
                      <div class="owl-nav">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </div>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                  </svg>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-6  ">
            <div class="box ">
              <div class="img-box">
                <img src="images/Raja-Ampat.jpg" alt="">
              </div>
              <div class="detail-box">
                <h5>
                  Raja Ampat
                </h5>
                <h6>
                  <span>Best Review</span>
                </h6>
                <a href="https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwjj0L-5t6H7AhXymGYCHVa0AYgYABAAGgJzbQ&ohost=www.google.com&cid=CAESa-D2T2o_1e-5zMV7s38ZECPkQM0K0Mg7oOCG4AZIi7i59g518wDQcDphYVJsXerpzQX7wOMr2bvf09SGoA6BuY_fsDldyVSXXYazEaeJfWeWbB5mY5amkHF2cf-F96Itmojg-GISWR53qebV&sig=AOD64_1i-pbFYXWOXjc2TbJ9nY1-dRpQRg&q&adurl&ved=2ahUKEwizrrm5t6H7AhXPSGwGHa1DDzoQ0Qx6BAgJEAE">
                  Lihat Selengkapnya... <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                    <!-- <g>
                      <g>
                        <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                     c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                      </g>
                    </g>
                    <g>
                      <g>
                        <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                     C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                     c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                     C457.728,97.71,450.56,86.958,439.296,84.91z" />
                      </g>
                    </g>
                    <g>
                      <g>
                        <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                     c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                      </g>
                    </g> -->
                    <g>
                      <div class="owl-nav">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </div>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                  </svg>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end offer section -->

  <!-- food section -->


  <section class="food_section layout_padding-bottom">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          Our Destinations
        </h2>
      </div>

      <ul class="filters_menu">
        <li class="active" data-filter="*">All</li>
        <li data-filter=".burger">Pulau Jawa</li>
        <li data-filter=".pizza">Pulau Sumatera</li>
        <li data-filter=".pasta">Pulau Bali</li>
        <li data-filter=".fries">Pulau Papua</li>
      </ul>

      <div class="filters-content">
        <div class="row grid">
          <?php
           $sumatera = "SELECT * FROM destinasi WHERE pulau = 'Pulau Sumatera'";
           $hasil = mysqli_query($conn, $sumatera);
           while ($row = mysqli_fetch_array($hasil)) :
           ?>
            <div class="col-sm-6 col-lg-4 all pizza">
              
              <div class="box">
                <div>
                  <div class="img-box">
                    <img src="image/<?= $row['gambar'] ?>" alt="">
                  </div>
                  <div class="detail-box">
                    <h5>
                      <?= $row['nama'] ?>
                    </h5>
                    <p>
                      <?= $row['deskripsi'] ?>
                    </p>
                    <div class="options">
                      <h6>
                        Lihat selanjutnya...
                      </h6>
                      <a href=<?= $row['link'] ?>>
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                          <div class="owl-nav">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                          </div>
                          <g>

                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile ?>
          <?php
           $jawa = "SELECT * FROM destinasi WHERE pulau = 'Pulau Jawa'";
           $hasil = mysqli_query($conn, $jawa);
           while ($row = mysqli_fetch_array($hasil)) :
           ?>
            <div class="col-sm-6 col-lg-4 all burger">
              
              <div class="box">
                <div>
                  <div class="img-box">
                    <img src="image/<?= $row['gambar'] ?>" alt="">
                  </div>
                  <div class="detail-box">
                    <h5>
                      <?= $row['nama'] ?>
                    </h5>
                    <p>
                      <?= $row['deskripsi'] ?>
                    </p>
                    <div class="options">
                      <h6>
                        Lihat selanjutnya...
                      </h6>
                      <a href=<?= $row['link'] ?>>
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                          <div class="owl-nav">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                          </div>
                          <g>

                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile ?>
          <?php
           $bali = "SELECT * FROM destinasi WHERE pulau = 'Pulau Bali'";
           $hasil = mysqli_query($conn, $bali);
           while ($row = mysqli_fetch_array($hasil)) :
           ?>
            <div class="col-sm-6 col-lg-4 all pasta">
              
              <div class="box">
                <div>
                  <div class="img-box">
                    <img src="image/<?= $row['gambar'] ?>" alt="">
                  </div>
                  <div class="detail-box">
                    <h5>
                      <?= $row['nama'] ?>
                    </h5>
                    <p>
                      <?= $row['deskripsi'] ?>
                    </p>
                    <div class="options">
                      <h6>
                        Lihat selanjutnya...
                      </h6>
                      <a href=<?= $row['link'] ?>>
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                          <div class="owl-nav">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                          </div>
                          <g>

                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile ?>
          <?php
           $papua = "SELECT * FROM destinasi WHERE pulau = 'Pulau Papua'";
           $hasil = mysqli_query($conn, $papua);
           while ($row = mysqli_fetch_array($hasil)) :
           ?>
            <div class="col-sm-6 col-lg-4 all fries">
              
              <div class="box">
                <div>
                  <div class="img-box">
                    <img src="image/<?= $row['gambar'] ?>" alt="">
                  </div>
                  <div class="detail-box">
                    <h5>
                      <?= $row['nama'] ?>
                    </h5>
                    <p>
                      <?= $row['deskripsi'] ?>
                    </p>
                    <div class="options">
                      <h6>
                        Lihat selanjutnya...
                      </h6>
                      <a href=<?= $row['link'] ?>>
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                          <div class="owl-nav">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                          </div>
                          <g>

                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile ?>
          
          
        </div>
      </div>
    </div>
    </div>

    <div class="btn-box">
      <a href="destinasi.php">
        View More
      </a>
    </div>
    </div>
  </section>

  <!-- end food section -->

  <!-- about section -->

  <section class="about_section layout_padding">
    <div class="container  ">

      <div class="row">
        <div class="col-md-6 ">
          <div class="img-box">
            <img src="images/id.jpg" alt="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="detail-box">
            <div class="heading_container">
              <h2>
                We Are NusantaraN
              </h2>
            </div>
            <p>
              Website as a forum for information related to culinary, tourist attractions, and souvenirs in Indonesia.
            </p>
            <a href="">
              Read More
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end about section -->

  <!-- book section -->
  <!-- <section class="book_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h2>
          Book A Table
        </h2>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form_container">
            <form action="">
              <div>
                <input type="text" class="form-control" placeholder="Your Name" />
              </div>
              <div>
                <input type="text" class="form-control" placeholder="Phone Number" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Your Email" />
              </div>
              <div>
                <select class="form-control nice-select wide">
                  <option value="" disabled selected>
                    How many persons?
                  </option>
                  <option value="">
                    2
                  </option>
                  <option value="">
                    3
                  </option>
                  <option value="">
                    4
                  </option>
                  <option value="">
                    5
                  </option>
                </select>
              </div>
              <div>
                <input type="date" class="form-control">
              </div>
              <div class="btn_box">
                <button>
                  Book Now
                </button>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-6">
          <div class="map_container ">
            <div id="googleMap"></div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <!-- end book section -->

  <!-- client section -->

  <section class="client_section layout_padding-bottom">
    <div class="container">
      <div class="heading_container heading_center psudo_white_primary mb_45">
        <h2>
          What Says Our Preview
        </h2>
      </div>
      <div class="carousel-wrap row ">
        <div class="owl-carousel client_owl-carousel">
          <div class="item">
            <div class="box">
              <div class="detail-box">
                <p>
                  "Informasi yang diberikan website NusantaraN ini sangat mudah perjalanan holiday keluarga saya"
                </p>
                <h6>
                  Nabila
                </h6>
                <p>
                  magna aliqua
                </p>
              </div>
              <div class="img-box">
                <img src="images/client1.jpg" alt="" class="box-img">
              </div>
            </div>
          </div>
          <div class="item">
            <div class="box">
              <div class="detail-box">
                <p>
                  "Website yang saya andalkan saat saya ingin pergi liburan.""
                </p>
                <h6>
                  Amir
                </h6>
                <p>
                  magna aliqua
                </p>
              </div>
              <div class="img-box">
                <img src="images/client2.jpg" alt="" class="box-img">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end client section -->

  <?php
  include_once 'layouts/footer.php';

  ?>

  <!-- jQery -->
  <script src="js/jquery-3.4.1.min.js"></script>
  <!-- popper js -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <!-- bootstrap js -->
  <script src="js/bootstrap.js"></script>
  <!-- owl slider -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
  </script>
  <!-- isotope js -->
  <script src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>
  <!-- nice select -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
  <!-- custom js -->
  <script src="js/custom.js"></script>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
  </script>
  <!-- End Google Map -->

</body>

</html>