<?php
include_once 'config.php';


if (isset($_POST['submit'])) {
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $pulau = $_POST['pulau'];
    $deskripsi = $_POST['deskripsi'];
    $gambar = $_FILES['gambar']['name'];
    $link =  $_POST['link'];


    if ($gambar != "") {
        $allowed_extensions = array("jpg", "jpeg", "png", "gif");
        $x = explode('.', $gambar);
        $extension = strtolower(end($x));
        $file_tmp = $_FILES['gambar']['tmp_name'];
        $imgnewfile = md5($gambar) . '-' . $gambar;

        if (in_array($extension, $allowed_extensions) === true) {
            $res = mysqli_query($conn, "SELECT * from destinasi WHERE id=$id limit 1");
            if ($row = mysqli_fetch_array($res)) {
                $deleteimage = $row['gambar'];
                unlink("../image/" . $deleteimage);
            }
            move_uploaded_file($file_tmp, "../image/" . $imgnewfile);
            $query  = "UPDATE destinasi SET nama = '$nama', deskripsi = '$deskripsi', pulau = '$pulau', link = '$link', gambar = '$imgnewfile' WHERE id='$id'";


            if (!$query) {
                die("Query gagal dijalankan: " . mysqli_errno($conn) .
                    " - " . mysqli_error($conn));
            } else {
                echo "<script>alert('Success!');window.location('admin.php')</script>";
            }
        } else {
            echo "<script>alert('Invalid format. Only jpg / jpeg/ png /gif format allowed');window.location('form.php');</script>";
        }
    } else {
        $query  = "UPDATE destinasi SET nama = '$nama', deskripsi = '$deskripsi', pulau = '$pulau', link = '$link' WHERE id='$id'";
        $result = mysqli_query($conn, $query);
        if (!$result) {
            die("Query gagal dijalankan: " . mysqli_errno($conn) .
                " - " . mysqli_error($conn));
        } else {
            echo "<script>alert('Data berhasil diubah.');window.location='admin.php';</script>";
        }
    }
}
