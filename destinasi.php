<?php
include_once 'layouts/header.php';
include_once 'functions/config.php';

?>


<nav class="navbar navbar-expand-lg custom_nav-container bg-dark mb-5">
    <div class="container">
        <a class="navbar-brand" href="index.html">
            <span>
                NusantaraN
            </span>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class=""> </span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav  mx-auto ">
                <li class="nav-item ">
                    <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="destinasi.php">Destinasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="about.php">About</a>
                </li>
            </ul>
            <div class="user_option">
                <a href="admin.php" class="user_link">
                    <i class="fa fa-user" aria-hidden="true"></i>
                </a>
                </a>
            </div>
        </div>

    </div>
</nav>


<body>
<section class="food_section layout_padding-bottom">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          Our Destinations
        </h2>
      </div>

      <ul class="filters_menu">
        <li class="active" data-filter="*">All</li>
        <li data-filter=".burger">Pulau Jawa</li>
        <li data-filter=".pizza">Pulau Sumatera</li>
        <li data-filter=".pasta">Pulau Bali</li>
        <li data-filter=".fries">Pulau Papua</li>
      </ul>

      <div class="filters-content">
        <div class="row grid">
          <?php
           $sumatera = "SELECT * FROM destinasi WHERE pulau = 'Pulau Sumatera'";
           $hasil = mysqli_query($conn, $sumatera);
           while ($row = mysqli_fetch_array($hasil)) :
           ?>
            <div class="col-sm-6 col-lg-4 all pizza">
              
              <div class="box">
                <div>
                  <div class="img-box">
                    <img src="image/<?= $row['gambar'] ?>" alt="">
                  </div>
                  <div class="detail-box">
                    <h5>
                      <?= $row['nama'] ?>
                    </h5>
                    <p>
                      <?= $row['deskripsi'] ?>
                    </p>
                    <div class="options">
                      <h6>
                        Lihat selanjutnya...
                      </h6>
                      <a href=<?= $row['link'] ?>>
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                          <div class="owl-nav">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                          </div>
                          <g>

                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile ?>
          <?php
           $jawa = "SELECT * FROM destinasi WHERE pulau = 'Pulau Jawa'";
           $hasil = mysqli_query($conn, $jawa);
           while ($row = mysqli_fetch_array($hasil)) :
           ?>
            <div class="col-sm-6 col-lg-4 all burger">
              
              <div class="box">
                <div>
                  <div class="img-box">
                    <img src="image/<?= $row['gambar'] ?>" alt="">
                  </div>
                  <div class="detail-box">
                    <h5>
                      <?= $row['nama'] ?>
                    </h5>
                    <p>
                      <?= $row['deskripsi'] ?>
                    </p>
                    <div class="options">
                      <h6>
                        Lihat selanjutnya...
                      </h6>
                      <a href=<?= $row['link'] ?>>
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                          <div class="owl-nav">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                          </div>
                          <g>

                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile ?>
          <?php
           $bali = "SELECT * FROM destinasi WHERE pulau = 'Pulau Bali'";
           $hasil = mysqli_query($conn, $bali);
           while ($row = mysqli_fetch_array($hasil)) :
           ?>
            <div class="col-sm-6 col-lg-4 all pasta">
              
              <div class="box">
                <div>
                  <div class="img-box">
                    <img src="image/<?= $row['gambar'] ?>" alt="">
                  </div>
                  <div class="detail-box">
                    <h5>
                      <?= $row['nama'] ?>
                    </h5>
                    <p>
                      <?= $row['deskripsi'] ?>
                    </p>
                    <div class="options">
                      <h6>
                        Lihat selanjutnya...
                      </h6>
                      <a href=<?= $row['link'] ?>>
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                          <div class="owl-nav">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                          </div>
                          <g>

                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile ?>
          <?php
           $papua = "SELECT * FROM destinasi WHERE pulau = 'Pulau Papua'";
           $hasil = mysqli_query($conn, $papua);
           while ($row = mysqli_fetch_array($hasil)) :
           ?>
            <div class="col-sm-6 col-lg-4 all fries">
              
              <div class="box">
                <div>
                  <div class="img-box">
                    <img src="image/<?= $row['gambar'] ?>" alt="">
                  </div>
                  <div class="detail-box">
                    <h5>
                      <?= $row['nama'] ?>
                    </h5>
                    <p>
                      <?= $row['deskripsi'] ?>
                    </p>
                    <div class="options">
                      <h6>
                        Lihat selanjutnya...
                      </h6>
                      <a href=<?= $row['link'] ?>>
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">

                          <div class="owl-nav">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                          </div>
                          <g>

                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile ?>
          </div>
      </div>
    </div>
    </div>

    <div class="btn-box">
      <a href="">
        View More
      </a>
    </div>
    </div>
  </section>
  <?php
  include_once 'layouts/footer.php';

  ?>

  <!-- jQery -->
  <script src="js/jquery-3.4.1.min.js"></script>
  <!-- popper js -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <!-- bootstrap js -->
  <script src="js/bootstrap.js"></script>
  <!-- owl slider -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
  </script>
  <!-- isotope js -->
  <script src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>
  <!-- nice select -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
  <!-- custom js -->
  <script src="js/custom.js"></script>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
  </script>
  <!-- End Google Map -->
  </body>