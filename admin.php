<?php

include_once 'layouts/admin/header.php';
include_once 'layouts/admin/navbar.php';
include_once 'layouts/admin/sidebar.php';

?>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Dashboard</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg connectedSortable">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Data Destinasi Wisata</h3>
          </div>
          <!-- /.card-header -->

          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Deskripsi</th>
                  <th>Pulau</th>
                  <th>Link</th>
                  <th>Gambar</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                include_once './functions/config.php';
                $query = "SELECT * FROM destinasi ORDER BY nama ASC";
                $result = mysqli_query($conn, $query);
                if (!$result) {
                  die("Query Error: " . mysqli_errno($conn) .
                    " - " . mysqli_error($conn));
                }

                $no = 1;
                while ($row = mysqli_fetch_assoc($result)) :
                ?>
                  <tr>
                    <td><?= $row['nama'] ?></td>
                    <td><?= $row['deskripsi'] ?>
                    </td>
                    <td><?= $row['pulau'] ?></td>
                    <td><a href="<?= $row['link'] ?>"><?= $row['link'] ?></a> </td>
                    <td style="text-align: center;"><img src="image/<?php echo $row['gambar']; ?>" style="width: 120px;"></td>
                    <td class="flex ">
                      <a href="edit.php?id=<?= $row['id']; ?>" class="btn btn-warning">Edit</a>
                      <form action="./functions/delete.php" method="POST">
                        <input name="delet" value="<?= $row['id']; ?>" hidden/>
                        <button class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus data ini?')" type="submit">Hapus</button>
                      </form>
                    </td>
                  </tr>
                <?php endwhile; ?>

              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->


    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<?php include_once 'layouts/admin/footer.php' ?>
</div>
<!-- ./wrapper -->
<?php include_once 'layouts/admin/script.php' ?>

</body>

</html>