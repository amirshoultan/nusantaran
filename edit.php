<?php

include_once 'layouts/admin/header.php';
include_once 'layouts/admin/navbar.php';
include_once 'layouts/admin/sidebar.php';
?>
<?php
include 'functions/config.php';

if (isset($_GET['id'])) {
    $id = ($_GET["id"]);

    $query = "SELECT * FROM destinasi WHERE id='$id'";
    $result = mysqli_query($conn, $query);
    if (!$result) {
        die("Query Error: " . mysqli_errno($conn) .
            " - " . mysqli_error($conn));
    }
    $data = mysqli_fetch_assoc($result);
    if (!count($data)) {
        echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
    }
} else {
    echo "<script>alert('Masukkan data id.');window.location='index.php';</script>";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Form Edit</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Form Data</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Tambah Data Destinasi Wisata</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="./functions/edit.php" method="post" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="form-group">
                                <input name="id" value="<?php echo $data['id']; ?>" hidden />
                                <label for="nama">Nama Destinasi Wisata</label>
                                <input type="text" class="form-control" id="nama" value="<?= $data['nama']; ?> " name="nama" placeholder="Masukkan Nama Destinasi Wisata">
                            </div>

                            <div class="form-group">
                                <label for="pulau">Pulau </label>
                                <select class="form-control " id="pulau" name="pulau">
                                    <option disabled>Please Select Option</option>
                                    <option value="Pulau 1" <?php if ($data['pulau'] == "Pulau Sumatera") echo 'selected="selected"'; ?>>Pulau Sumatera</option>
                                    <option value="Pulau 2" <?php if ($data['pulau'] == "Pulau Jawa") echo 'selected="selected"'; ?>>Pulau Jawa</option>
                                    <option value="Pulau 3" <?php if ($data['pulau'] == "Pulau Bali") echo 'selected="selected"'; ?>>Pulau Bali</option>
                                    <option value="Pulau 3" <?php if ($data['pulau'] == "Pulau Papua") echo 'selected="selected"'; ?>>Pulau Papua</option>
                                </select>

                            </div>

                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea class="form-control" rows="3" placeholder="Masukkan deskripsi..." name="deskripsi"><?= $data['deskripsi'] ?></textarea>
                            </div>

                            <div class="form-group">
                                <label for="link">Link Destinasi Wisata</label>
                                <input type="text" class="form-control" id="link" name="link" value="<?= $data['link']; ?>" placeholder=" Masukkan Nama Destinasi Wisata">
                            </div>

                            <div class="form-group">
                                <label for="gambar">File input</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="gambar" name="gambar">
                                        <label class="custom-file-label" for="gambar">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Upload</span>
                                    </div>
                                </div>
                                <img src="./image/<?= $data['gambar']; ?>" class="w-25 p-3 ">
                            </div>

                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->

                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once 'layouts/admin/footer.php' ?>
</div>
<!-- ./wrapper -->
</div>
<!-- ./wrapper -->
<?php include_once 'layouts/admin/script.php' ?>

<!-- Page specific script -->
<script>
    $(function() {
        bsCustomFileInput.init();
    });
</script>
</body>

</html>